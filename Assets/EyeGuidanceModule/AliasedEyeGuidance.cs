using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using System;

[Serializable, VolumeComponentMenu("Post-processing/Custom/AliasedEyeGuidance")]
public sealed class AliasedEyeGuidance : CustomPostProcessVolumeComponent, IPostProcessComponent
{
    [Tooltip("Controls the intensity of the effect.")]
    public ClampedFloatParameter intensity = new ClampedFloatParameter(0f, 0f, 1f);

    [Tooltip("The position of the viewer's eye on the display, with 0,0 being the center, and extending from {-1,-1} to {1,1}")]
    public Vector2Parameter eyePosition = new Vector2Parameter(Vector2.zero);

    [Tooltip("The 2D normal from the eyePosition towards the target")]
    public Vector2Parameter targetDelta = new Vector2Parameter(Vector2.zero);

    [Tooltip("Distance from the view position that the aliasing should be rendered")]
    public ClampedFloatParameter aliasDist = new ClampedFloatParameter(0.1f, 0f, 1f);

    [Tooltip("Radius of the effected area")]
    public ClampedFloatParameter EffectRadius = new ClampedFloatParameter(0.01f, 0f, 1.0f);

    [Tooltip("Controls the size of the artificial aliasing grid, in pixels")]
    public ClampedIntParameter GridScale = new ClampedIntParameter(1, 1, 64);

    [Tooltip("Scales time related effects")]
    public ClampedFloatParameter TimeScaling = new ClampedFloatParameter(0.01f, 0.0f, 10.0f);

    Material m_Material;

    public bool IsActive() => m_Material != null && intensity.value > 0f;

    // Do not forget to add this post process in the Custom Post Process Orders list (Project Settings > HDRP Default Settings).
    public override CustomPostProcessInjectionPoint injectionPoint => CustomPostProcessInjectionPoint.AfterPostProcess;

    const string kShaderName = "Hidden/Shader/AliasedEyeGuidance";

    public override void Setup()
    {
        if (Shader.Find(kShaderName) != null)
            m_Material = new Material(Shader.Find(kShaderName));
        else
            Debug.LogError($"Unable to find shader '{kShaderName}'. Post Process Volume AliasedEyeGuidance is unable to load.");
    }

    public override void Render(CommandBuffer cmd, HDCamera camera, RTHandle source, RTHandle destination)
    {
        if (m_Material == null)
            return;

        m_Material.SetInt("_GridScale", GridScale.value);
        m_Material.SetFloat("_Intensity", intensity.value);
        m_Material.SetFloat("_AliasDist", aliasDist.value);
        m_Material.SetFloat("_EffectRadius", EffectRadius.value);
        m_Material.SetFloat("_TimeScaling", TimeScaling.value);
        m_Material.SetVector("_EyePosition", eyePosition.value);
        m_Material.SetVector("_TargetDelta", targetDelta.value);
        m_Material.SetTexture("_InputTexture", source);
        HDUtils.DrawFullScreen(cmd, m_Material, destination);
    }

    public override void Cleanup()
    {
        CoreUtils.Destroy(m_Material);
    }
}
