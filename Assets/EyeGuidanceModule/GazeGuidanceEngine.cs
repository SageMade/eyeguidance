using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeGuidanceEngine : MonoBehaviour
{
    public UnityEngine.Rendering.Volume GuidanceVolume;
    public AliasedEyeGuidance EyeGuidanceEffect;
    public GazeInput GazeInputProvider;
    public float AngleCutoff = 35.0f;
    public float AngleBlend = 5.0f;

    public GameObject Target;

    // Start is called before the first frame update
    void Start()
    {
        if (EyeGuidanceEffect == null && GuidanceVolume != null) {
            bool found = GuidanceVolume.profile.TryGet<AliasedEyeGuidance>(out EyeGuidanceEffect);
            if (!found) {
                Debug.LogWarning("Failed to find the aliased eye guidance effect in the volume");
            }
        }

        if (GazeInputProvider == null) {
            GazeInputProvider = FindObjectOfType<GazeInput>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEngine.InputSystem.Keyboard.current.cKey.wasPressedThisFrame) {
            GazeInputProvider.RunCalibration();
        }

        if (EyeGuidanceEffect != null && GazeInputProvider != null) {
            EyeGuidanceEffect.active = Target != null;

            if (Target != null) {
                Transform h = GazeInputProvider.HeadTransform;

                Vector2 pos = GazeInputProvider.GetEyePos(ViveSR.anipal.Eye.EyeIndex.LEFT);
                EyeGuidanceEffect.eyePosition.value = pos;

                Vector3 toTarget = Vector3.Normalize(Target.transform.position - (h.position + h.forward));
                Vector2 dir = new Vector2(Vector3.Dot(toTarget, h.right), Vector3.Dot(toTarget, h.up)).normalized;

                Debug.DrawLine(h.position, h.position + h.forward, Color.green);
                Debug.DrawLine(h.position + h.forward, h.position + h.forward + toTarget, Color.blue);
                Debug.DrawLine(h.position, h.position + h.TransformDirection(new Vector3(dir.x, dir.y, 0.0f)), Color.cyan);

                float a = Mathf.Acos(Vector3.Dot(h.forward, toTarget)) * Mathf.Rad2Deg;

                float power = Mathf.SmoothStep(0.0f, 1.0f, Mathf.Clamp01(Mathf.Max(a - AngleCutoff, 0.0f) / AngleBlend));
                EyeGuidanceEffect.intensity.value = power;   
                 
                EyeGuidanceEffect.targetDelta.value = dir;
            }
        }
    }
}
