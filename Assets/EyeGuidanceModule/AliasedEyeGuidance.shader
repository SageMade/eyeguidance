Shader "Hidden/Shader/AliasedEyeGuidance"
{
    HLSLINCLUDE

    #pragma target 4.5
    #pragma only_renderers d3d11 playstation xboxone xboxseries vulkan metal switch

    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/FXAA.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/RTUpscale.hlsl"

    struct Attributes
    {
        uint vertexID : SV_VertexID;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };

    struct Varyings
    {
        float4 positionCS : SV_POSITION;
        float2 texcoord   : TEXCOORD0;
        UNITY_VERTEX_OUTPUT_STEREO
    };

    Varyings Vert(Attributes input)
    {
        Varyings output;
        UNITY_SETUP_INSTANCE_ID(input);
        UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
        output.positionCS = GetFullScreenTriangleVertexPosition(input.vertexID);
        output.texcoord = GetFullScreenTriangleTexCoord(input.vertexID);
        return output;
    }

    // List of properties to control your post process effect
    float _Intensity;
    TEXTURE2D_X(_InputTexture);

    float _AliasDist;
    float _EffectRadius;

    float2 _EyePosition;
    float2 _TargetDelta;

    int _GridScale;
    float _TimeScaling;

    float3 RGBToHSV(float3 c)
	{
		float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
		float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
		float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
		float d = q.x - min( q.w, q.y );
		float e = 1.0e-10;
		return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
	}

	float3 HSVToRGB( float3 c )
	{
		float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
		float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
		return c.z * lerp( K.xxx, saturate( p - K.xxx ), c.y );
	}

    float4 CustomPostProcess(Varyings input) : SV_Target
    {
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
    
        uint2 positionRaw     = input.texcoord * _ScreenSize.xy;  
        float3 outColor = LOAD_TEXTURE2D_X(_InputTexture, positionRaw).xyz;



        float2 positionNormal = input.texcoord - float2(0.5, 0.5);
        positionNormal *= 2.0;

        float2 aliasPos = _EyePosition + normalize(_TargetDelta) * _AliasDist;
        float2 offset = float2(
            (cos(_Time.y * _TimeScaling) * _EffectRadius),// / _ScreenSize.x,
            (sin(_Time.y * _TimeScaling) * _EffectRadius) // / _ScreenSize.y
        );
        uint2 positionAliased = offset + round(
            (input.texcoord * 
            _ScreenSize.xy) / _GridScale
        ) * _GridScale;  

        float3 aliasedColor = LOAD_TEXTURE2D_X(_InputTexture, positionAliased).xyz;

        float intensity = max(1 - (distance(aliasPos, positionNormal) / _EffectRadius), 0) * _Intensity;

        float3 mixedColor = lerp(outColor, aliasedColor, intensity);

        float3 derivX = ddx(mixedColor);

        float3 hsv = RGBToHSV(mixedColor);
        //hsv.r = fmod(hsv.r + temp / 10.0f, 1.0);
        hsv.b *= 0.5f;

        return float4(saturate(lerp(mixedColor, HSVToRGB(hsv), intensity)), 1.0f);
    }

    ENDHLSL

    SubShader
    {
        Pass
        {
            Name "AliasedEyeGuidance"

            ZWrite Off
            ZTest Always
            Blend Off
            Cull Off

            HLSLPROGRAM
                #pragma fragment CustomPostProcess
                #pragma vertex Vert
            ENDHLSL
        }
    }
    Fallback Off
}
