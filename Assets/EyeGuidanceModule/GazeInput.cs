using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

using ViveSR.anipal.Eye;

public class GazeInput : MonoBehaviour {
    public float PollRate = 1.0f / 60.0f; // Poll 10 times per second (a saccade is ~100ms)
    public Transform HeadTransform;

    [System.Serializable]
    struct GazeData {
        public Ray LeftEyeRay;
        public Ray RightEyeRay;
        public Ray CombinedEyeRay;
        public Vector2 LeftEyePos;
        public Vector2 RightEyePos;
    }
    GazeData _gazeData;
    private static EyeData_v2 _eyeData = new EyeData_v2();
    private bool _eyeCallbackRegistered = false;

    // Start is called before the first frame update
    void Start() {
        if (SRanipal_Eye_Framework.Instance == null || !SRanipal_Eye_Framework.Instance.EnableEye) {
            //enabled = false;
            Debug.LogErrorFormat("This HMD does not support eye tracking!");
            return;
        } else {
            System.IntPtr callback = Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback);
            SRanipal_Eye_v2.WrapperRegisterEyeDataCallback(callback);
        }

        if (HeadTransform == null) {
            HeadTransform = Camera.main.transform;
        }
    }

    private void OnDestroy() {
        StartCoroutine(Release());
    }

    // Update is called once per frame
    void Update() {
        // Early abort if the SRanipal framework is not current online
        if (SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.WORKING &&
            SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.NOT_SUPPORT) 
            return;

        // Extract pupil positions
        SRanipal_Eye_v2.GetPupilPosition(EyeIndex.LEFT, out _gazeData.LeftEyePos);
        SRanipal_Eye_v2.GetPupilPosition(EyeIndex.RIGHT, out _gazeData.RightEyePos);

        // Extract gaze rays
        Vector3 origin, dir;
        SRanipal_Eye_v2.GetGazeRay(GazeIndex.LEFT, out origin, out dir);
        _gazeData.LeftEyeRay = new Ray(origin, dir);
        SRanipal_Eye_v2.GetGazeRay(GazeIndex.LEFT, out origin, out dir, _eyeData);
        _gazeData.RightEyeRay = new Ray(origin, dir);
        SRanipal_Eye_v2.GetGazeRay(GazeIndex.COMBINE, out origin, out dir, _eyeData);
        _gazeData.CombinedEyeRay = new Ray(origin, dir);
    }

    public void RunCalibration() {
       SRanipal_Eye_v2.LaunchEyeCalibration();
    }

    /// <summary>
    /// Gets the position of the given eye, in the [-1,1] range
    /// [-1,-1] is bottom left corner, [1,1] is upper right corner
    /// </summary>
    /// <param name="index">The eye to get the position for</param>
    public Vector2 GetEyePos(EyeIndex index) {
        switch (index) {
            case EyeIndex.LEFT:
                return _gazeData.LeftEyePos;
            case EyeIndex.RIGHT:
                return _gazeData.RightEyePos;
            default:
                return Vector2.zero;

        }
    }

    /// <summary>
    /// Performs a physics raycast using the specified gaze ray
    /// </summary>
    /// <param name="index">The gaze provider to use (left eye, right eye, or combined)</param>
    /// <returns>The gameobject that the raycast intersects with, or null if no object was detected</returns>
    public GameObject Raycast(GazeIndex index = GazeIndex.COMBINE) {
        Ray ray;
        switch(index) {
            case GazeIndex.LEFT:
                ray = _gazeData.LeftEyeRay;
                break;
            case GazeIndex.RIGHT:
                ray = _gazeData.RightEyeRay;
                break;
            case GazeIndex.COMBINE:
                ray = _gazeData.CombinedEyeRay;
                break;
            default:
                return null;

        }

        // Transform the eye position based on the player's head
        ray.origin = HeadTransform.TransformPoint(ray.origin);
        ray.direction = HeadTransform.TransformDirection(ray.direction);

        RaycastHit rayHit;
        if (Physics.Raycast(ray, out rayHit)) {
            return rayHit.transform.gameObject;
        }
        else {
            return null;
        }
    }


    private IEnumerator Release() {
        if (_eyeCallbackRegistered == true) {
            _eyeCallbackRegistered = false;
            Invoke("SRanipal_Release", 0.0f);
            yield return new WaitForSecondsRealtime(2.0f);
            CancelInvoke("SRanipal_Release");
        }
    }

    private void SRanipal_Release() {
        SRanipal_Eye_v2.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
    }

    private static void EyeCallback(ref EyeData_v2 eye_data) {
        _eyeData = eye_data;
    }
}
