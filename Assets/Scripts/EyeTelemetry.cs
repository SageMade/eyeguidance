﻿//========= Copyright 2018, HTC Corporation. All rights reserved. ===========
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Assertions;

namespace ViveSR {
    namespace anipal {
        namespace Eye {
            public class EyeTelemetry : MonoBehaviour {
                [SerializeField]
                public string OutputFilePath = "eye_data_{0}.csv";
                public float PollRate = 1.0f / 10.0f; // Poll 10 times per second (a saccade is ~100ms)
                // Our default gaze cone is 6 degrees to either side, which matches approximately to the foveal radii
                public float GazeCone = 6.0f; 
                private static EyeData_v2 eyeData = new EyeData_v2();
                private bool eye_callback_registered = false;

                public bool WriteData = true;

                public bool ShowDebugRays = true;
                public int DebugConeSegments = 16;

                private StreamWriter output_file;

                private float timer = 0.0f;
                private Transform[] gazeTarget;
                                
                private void Start() {
                    if (!SRanipal_Eye_Framework.Instance.EnableEye) {
                        enabled = false;
                        return;
                    }
                    if (WriteData) {
                        output_file = new StreamWriter(string.Format(OutputFilePath, System.DateTime.Now.ToString("yyyy_mm_dd_HH_mm_ss")), false);
                        output_file.WriteLine("Stream, Timestamp, origin_x, origin_y, origin_z, dir_z, dir_y, dir_z, tag, name, metadata");
                    }
                    Debug.Log(Mathf.Deg2Rad * GazeCone);
                    gazeTarget = new Transform[3];
                }

                private void Update()
                {
                    // Early abort if the SRanipal framework is not current online
                    if (SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.WORKING &&
                        SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.NOT_SUPPORT) return;

                    // Handle registering or unregistering our data callback with the framework, in case the framework disable the eye callback
                    if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == true && eye_callback_registered == false) {
                        SRanipal_Eye_v2.WrapperRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
                        eye_callback_registered = true; 
                    }
                    else if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == false && eye_callback_registered == true) {
                        SRanipal_Eye_v2.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
                        eye_callback_registered = false;
                    }

                    // Increment our poll timer
                    timer += Time.deltaTime;  

                    // Only log our eye data on polling events
                    if (timer > PollRate) {
                        timer -= PollRate;

                        GazeTelemetry(ref gazeTarget[0]);
                        HeadsetRayTelemetry(ref gazeTarget[1]);
                        HeadsetConeTelemetry(ref gazeTarget[2], GazeCone);
                    }

                    if (ShowDebugRays) {
                        // Display the headset ray
                        Debug.DrawLine(Camera.main.transform.position, Camera.main.transform.position + Camera.main.transform.forward * Camera.main.farClipPlane, Color.red);

                        // Display the edges of the cone
                        Vector3 endRay = new Vector3(0, 0, Camera.main.farClipPlane);
                        float farEdge = Mathf.Sin(GazeCone * Mathf.Deg2Rad) * Camera.main.farClipPlane;
                        float step = (Mathf.PI * 2.0f) / DebugConeSegments;
                        for (int ix = 0; ix < DebugConeSegments; ix++)  {
                            Vector3 offset = new Vector3(Mathf.Cos(ix * step) * farEdge, Mathf.Sin(ix * step) * farEdge, 0);
                            Debug.DrawLine(Camera.main.transform.position, Camera.main.transform.TransformDirection(endRay + offset), Color.green);
                        }

                        // Display the gaze tracking ray
                        // Extract all data from the SRapinal structure
                        Vector3 combinedOrigin, combinedDirection;
                        SRanipal_Eye_v2.GetGazeRay(GazeIndex.COMBINE, out combinedOrigin, out combinedDirection, eyeData);

                        // Transform all the local transforms into the world coordinate system for logging
                        combinedDirection = Camera.main.transform.TransformDirection(combinedDirection);
                        combinedOrigin = Camera.main.transform.TransformPoint(combinedOrigin);

                        Debug.DrawLine(combinedOrigin, combinedDirection * Camera.main.farClipPlane, Color.cyan);
                    }
                }

                /// <summary>
                /// Handles telemetry and calculations using the Tobii eye tracking data
                /// </summary>
                /// <param name="currentTarget">The last known object to be the gaze target when using this algorithm</param>
                private void GazeTelemetry(ref Transform currentTarget) {
                    // Extract all data from the SRapinal structure
                    Vector3 combinedOrigin, combinedDirection;
                    SRanipal_Eye_v2.GetGazeRay(GazeIndex.COMBINE, out combinedOrigin, out combinedDirection, eyeData);

                    // Transform all the local transforms into the world coordinate system for logging
                    combinedDirection = Camera.main.transform.TransformDirection(combinedDirection);
                    combinedOrigin = Camera.main.transform.TransformPoint(combinedOrigin);

                    RaycastHit rayHit;

                    // Use our gaze information to project a ray into the scene to determine what we are looking at
                    Physics.Raycast(combinedOrigin, combinedDirection, out rayHit, Camera.main.farClipPlane);

                    // We'll only log when the object we are looking at has changed
                    if (rayHit.transform != currentTarget) {
                        currentTarget = rayHit.transform;
                        WriteHitInformation(currentTarget, "Gaze", combinedOrigin, combinedDirection);
                    }
                }

                /// <summary>
                /// Handles telemetry and calculations using a headset raycast
                /// </summary>
                /// <param name="currentTarget">The last known object to be the gaze target when using this algorithm</param>
                private void HeadsetRayTelemetry(ref Transform currentTarget) {
                    RaycastHit rayHit;
                    // Use our headset's position to project a ray into the scene to determine what we are looking at
                    Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out rayHit, Camera.main.farClipPlane);

                    // We'll only log when the object we are looking at has changed
                    if (rayHit.transform != currentTarget) {
                        currentTarget = rayHit.transform;

                        WriteHitInformation(currentTarget, "HeadRay", Camera.main.transform.position, Camera.main.transform.forward);
                    }
                }

                /// <summary>
                /// Handles telemetry and calculations using a headset cone cast, with the given angle in degrees
                /// </summary>
                /// <param name="currentTarget">The last known object to be the gaze target when using this algorithm</param>
                /// <param name="coneDeg">The angle of the cone cast, in degrees</param>
                private void HeadsetConeTelemetry(ref Transform currentTarget, float coneDeg) {
                    GameObject target = null;
                    float minDot = float.MaxValue;
                    Vector3 eyePos = Camera.main.transform.position;
                    var gazeItems = Component.FindObjectsOfType<GazeItem>();
                    foreach (var item in gazeItems) {
                        float dot = Vector3.Dot(Vector3.Normalize(item.gameObject.transform.position - eyePos), Vector3.Normalize(Camera.main.transform.forward));
                        // Since A * B = cos(θ)|A||B|, and |A| and |B| are 1, we can extract θ by taking the acos of the dot product
                        // Since this angle is in radians, we convert one side before testing
                        if ((Mathf.Acos(dot) < Mathf.Deg2Rad * coneDeg) && dot < minDot) {
                            minDot = dot;
                            target = item.gameObject;
                        }
                    }

                    if (target != currentTarget?.gameObject) {
                        currentTarget = target?.transform;
                        WriteHitInformation(currentTarget, "HeadCone", Camera.main.transform.position, Camera.main.transform.forward, coneDeg);
                    }
                }

                private void WriteHitInformation(Transform target, string stream, Vector3 origin, Vector3 normal, object metadata = null) {
                    // Our object tag will store the tag of the object being looked at
                    string objectTag = "NULL";
                    // objectName will store the name of the object being looked at
                    string objectName = "NULL";

                    // Extract name or tag information from the raycast results
                    objectTag = target?.tag ?? "NULL";
                    objectName = target?.name ?? "NULL";

                    // Determine our timestamp for the entry, and log the gaze event
                    float time = Time.timeSinceLevelLoad;
                    if (WriteData) {
                        output_file.Write(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}\n", stream, time,
                        origin.x, origin.y, origin.z,
                        normal.x, normal.y, normal.z,
                        objectTag, objectName,
                        metadata ?? ""));
                        output_file.Flush();
                    }
                }

                private void Release()
                {
                    if (eye_callback_registered == true) {
                        SRanipal_Eye_v2.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
                        eye_callback_registered = false;
                    }
                    if (WriteData) {
                        output_file.Close();
                    }
                }

                private static void EyeCallback(ref EyeData_v2 eye_data)
                {
                    eyeData = eye_data;
                }
            }
        }
    }
}
