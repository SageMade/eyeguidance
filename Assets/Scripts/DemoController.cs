using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DemoController : MonoBehaviour
{
    public List<GameObject> Targets;
    public UnityEngine.Rendering.Volume GuidanceVolume;

    public Camera Cam;
    public GazeGuidanceEngine GazeEngine;

    private AliasedEyeGuidance _guidance;

    GameObject targetObj;

    Vector2 _virtualGaze;
    Vector2 _virtualTargetPos;

    public RectTransform Test;
    public AudioSource Audio;

    public bool UseGuidance = true;

    // Start is called before the first frame update
    void Start()
    {
        if (Targets.Count < 2) {
            Debug.LogWarning("Must have at least 2 targets!");
            enabled = false;
            return;
        }

        if (!GuidanceVolume.profile.TryGet(out _guidance)) {
            Debug.LogWarning("Failed to find aliased eye guidance shader");
            enabled = false;
            return;
        }

        if (Audio == null) {
            Audio = GetComponent<AudioSource>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (targetObj == null) {
            _SelectTarget();
        }

        if (GazeEngine.GazeInputProvider.Raycast() == targetObj) {
            _SelectTarget();
            Debug.Log("Changing targets!");
            Audio?.Play();
        }

        /*
        if (Keyboard.current.spaceKey.wasPressedThisFrame) {
            UseGuidance = !UseGuidance;
        }
        _virtualGaze = Cam.ScreenToViewportPoint(Mouse.current.position.ReadValue());

        Ray ray = Cam.ViewportPointToRay(new Vector3(_virtualGaze.x, _virtualGaze.y, Cam.nearClipPlane));
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * 100, Color.yellow);
        RaycastHit rayHit;
        if (Physics.Raycast(ray, out rayHit, Cam.farClipPlane, 1 << 7)) {
            if (rayHit.transform.gameObject == targetObj) {
                _SelectTarget();
                Debug.Log("Changing targets!");
                Audio?.Play();
            }
        }

        _guidance.active = UseGuidance;
        if (UseGuidance) {
            _guidance.eyePosition.value = _ToNDC(_virtualGaze);
            _guidance.targetDelta.value = (_ToNDC(_virtualTargetPos) - _ToNDC(_virtualGaze)).normalized;
        }
        */
    }

    void _SelectTarget() {
        GameObject target = null;
        do {
            target = Targets[Random.Range(0, Targets.Count)];
        }
        while (target == targetObj);
        targetObj = target;
        GazeEngine.Target = target;

        /*
        Vector3 pos = _GetBounds(targetObj).center;
        _virtualTargetPos = Cam.WorldToViewportPoint(pos);
        if (Test != null) {
            Test.anchoredPosition = Cam.ViewportToScreenPoint(_virtualTargetPos);
        }
        */
    }

    Bounds _GetBounds(GameObject go) {
        MeshRenderer renderer;
        Bounds combined = go.TryGetComponent<MeshRenderer>(out renderer) ? renderer.bounds : new Bounds(go.transform.position, Vector3.one);

        MeshRenderer[] renderers = go.GetComponentsInChildren<MeshRenderer>();
        foreach (Renderer childRender in renderers) {
            if (childRender != renderer) combined.Encapsulate(childRender.bounds);
        }
        return combined;
    }

    Vector2 _ToNDC(Vector2 viewPort) {
        return (viewPort * 2.0f) - Vector2.one;
    }
}
