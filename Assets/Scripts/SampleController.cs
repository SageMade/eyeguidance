using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleController : MonoBehaviour
{
    public GazeGuidanceEngine GazeEngine;
    public GameObject Sphere;

    public float Radius = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        if (Sphere == null) {
            enabled = false;
        }
        if (GazeEngine == null) {
            GazeEngine = FindObjectOfType<GazeGuidanceEngine>();
            if (GazeEngine != null) {
                GazeEngine.Target = Sphere;
            }
        }

        Vector3 pos = Random.onUnitSphere;
        Sphere.transform.position = new Vector3(pos.x, 3.0f, pos.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (GazeEngine.GazeInputProvider.Raycast() == Sphere) {
            Vector3 pos = Random.onUnitSphere * Radius;
            Sphere.transform.position = new Vector3(pos.x, 3.0f, pos.z);
        }
    }
}
